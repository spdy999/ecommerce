## Installation

```bash
$ yarn
```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev

```

## Postman Document

https://www.getpostman.com/collections/6b13f0c7259913d02a20
