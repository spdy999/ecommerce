import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SignUpDTO } from 'src/auth/auth.dto';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findOne(username: string): Promise<User | undefined> {
    return this.usersRepository.findOne({ username });
  }

  async insert(user: SignUpDTO): Promise<User> {
    const userEntity: User = User.create();
    const { username, password } = user;
    userEntity.username = username;
    userEntity.password = password;

    await User.save(userEntity);

    return userEntity;
  }

  async findOrderHistory(username: string): Promise<User> {
    const orderHistory = await this.usersRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.orders', 'order')
      .innerJoinAndSelect('order.productOrders', 'productOrders')
      .innerJoinAndSelect('productOrders.product', 'product')
      .where('user.username = :username', { username })
      .getOne();
    // console.log(JSON.stringify(orderHistory, null, 4));

    return orderHistory;
  }

  async findOrderById(username: string, orderId: number): Promise<User> {
    const order = await this.usersRepository
      .createQueryBuilder('user')
      .innerJoinAndSelect('user.orders', 'order')
      .innerJoinAndSelect('order.productOrders', 'productOrders')
      .innerJoinAndSelect('productOrders.product', 'product')
      .where('user.username = :username AND order.id = :id', {
        username,
        id: orderId,
      })
      .getOne();

    return order;
  }
}
