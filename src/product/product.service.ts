import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  async findAll(): Promise<Product[]> {
    return this.productsRepository.find();
  }

  async findOne(id: number): Promise<Product> {
    return this.productsRepository.findOne(id);
  }
  async findByIds(ids: number[]): Promise<Product[]> {
    return this.productsRepository.findByIds(ids);
  }
  async groupByIds(ids: number[]): Promise<any> {
    return this.productsRepository
      .createQueryBuilder('product')
      .whereInIds(ids)
      .groupBy('product.id')
      .getRawMany();
  }
}
