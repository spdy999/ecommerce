import { ProductOrder } from 'src/products-orders/product-order.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  OneToMany,
} from 'typeorm';

@Entity()
export class Product extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  price: number;

  @OneToMany((type) => ProductOrder, (productOrder) => productOrder.product)
  productOrders: ProductOrder[];
}
