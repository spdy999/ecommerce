import { Order } from 'src/orders/order.entity';
import { Product } from 'src/product/product.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
} from 'typeorm';

@Entity()
export class ProductOrder extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  productQuantity: number;

  @ManyToOne((type) => Product, (product) => product.productOrders, {
    primary: true,
    onDelete: 'CASCADE',
  })
  product: Product;

  @ManyToOne((type) => Order, (order) => order.productOrders, {
    primary: true,
    onDelete: 'CASCADE',
  })
  order: Order;
}
