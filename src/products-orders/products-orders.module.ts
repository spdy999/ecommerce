import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductOrder } from './product-order.entity';
import { ProductsOrdersService } from './products-orders.service';

@Module({
  imports: [TypeOrmModule.forFeature([ProductOrder])],
  providers: [ProductsOrdersService],
  exports: [ProductsOrdersService],
})
export class ProductsOrdersModule {}
