import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order } from 'src/orders/order.entity';
import { Product } from 'src/product/product.entity';
import { Repository } from 'typeorm';
import { ProductOrder } from './product-order.entity';

interface ProductOrderObj {
  product: Product;
  productQuantity: number;
}
@Injectable()
export class ProductsOrdersService {
  constructor(
    @InjectRepository(ProductOrder)
    private productOrdersRepository: Repository<ProductOrder>,
  ) {}

  async create(
    product: Product,
    order: Order,
    productQuantity: number,
  ): Promise<ProductOrder> {
    const productOrderEntity: ProductOrder = ProductOrder.create();
    productOrderEntity.productQuantity = productQuantity;
    productOrderEntity.order = order;
    productOrderEntity.product = product;
    await ProductOrder.save(productOrderEntity);
    return productOrderEntity;
  }

  async createMany(
    productOrders: ProductOrderObj[],
    savedOrder: Order,
  ): Promise<any> {
    return productOrders.map(async (productOrder: ProductOrderObj) => {
      await this.create(
        productOrder.product,
        savedOrder,
        productOrder.productQuantity,
      );
    });
  }

  async getProductsByOrderId(order: Order): Promise<ProductOrder[]> {
    return this.productOrdersRepository.find({
      relations: ['product'],
      where: { order },
    });
  }
}
