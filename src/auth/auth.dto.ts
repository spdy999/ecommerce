export interface SignUpDTO {
  username: string;
  password: string;
}

export interface JwtUserPayloadDTO {
  username: string;
  id: number;
}
