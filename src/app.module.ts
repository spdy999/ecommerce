import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { User } from './users/user.entity';
import { ProductModule } from './product/product.module';
import { Product } from './product/product.entity';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/order.entity';
import { ProductOrder } from './products-orders/product-order.entity';
import { ProductsOrdersModule } from './products-orders/products-orders.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: 'HelloWorld123!',
      database: 'neversitup',
      entities: [User, Product, Order, ProductOrder],
      synchronize: true,
    }),
    ProductModule,
    OrdersModule,
    ProductsOrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
