import {
  ClassSerializerInterceptor,
  Delete,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { Body } from '@nestjs/common';
import { Controller, Get, Request, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { ProductsOrdersService } from 'src/products-orders/products-orders.service';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { DeleteResult } from 'typeorm';
import { CreateUserOrderDTO, ProductOrderDTO } from './order.dto';
import { OrdersService } from './orders.service';

@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(JwtAuthGuard)
@Controller('orders')
export class OrdersController {
  constructor(
    private orderService: OrdersService,
    private usersService: UsersService,
    private productsOrdersService: ProductsOrdersService,
  ) {}

  @Get()
  async listUserOrders(@Request() req): Promise<User> {
    const userOrders = await this.usersService.findOrderHistory(
      req.user.username,
    );
    return userOrders;
  }

  // TODO: Protect if not belong to user
  @Get(':id')
  async getOrderById(@Request() req, @Param() params): Promise<User> {
    const userOrder = await this.usersService.findOrderById(
      req.user.username,
      params.id,
    );
    return userOrder;
  }

  @Post()
  async createUserOrder(
    @Request() req,
    @Body() productOrder: CreateUserOrderDTO,
  ) {
    const user = await this.usersService.findOne(req.user.username);

    return this.orderService.createUserOrder(productOrder, user);
  }

  @Delete(':id')
  async deleteOrderById(@Param('id') id: string): Promise<DeleteResult> {
    return this.orderService.deleteOrder(id);
  }
}
