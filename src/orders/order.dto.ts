export interface ProductOrderDTO {
  id: number;
  quantity: number;
}

export interface CreateUserOrderDTO {
  products: ProductOrderDTO[];
}
