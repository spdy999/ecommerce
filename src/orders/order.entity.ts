import { ProductOrder } from 'src/products-orders/product-order.entity';
import { User } from 'src/users/user.entity';
import { Exclude } from 'class-transformer';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  ManyToOne,
  OneToMany,
} from 'typeorm';

@Entity()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  totalPrice: number;

  @Exclude()
  @ManyToOne(() => User, (user) => user.orders, { nullable: false })
  user: User;

  @OneToMany((type) => ProductOrder, (productOrder) => productOrder.order)
  productOrders: ProductOrder[];
}
