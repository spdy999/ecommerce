import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/product/product.entity';
import { ProductService } from 'src/product/product.service';
import { ProductsOrdersService } from 'src/products-orders/products-orders.service';
import { User } from 'src/users/user.entity';
import { DeleteResult, Repository } from 'typeorm';
import { CreateUserOrderDTO, ProductOrderDTO } from './order.dto';
import { Order } from './order.entity';

interface ProductOrderObj {
  product: Product;
  productQuantity: number;
}
interface OrderObject {
  totalPrice: number;
  productOrders: ProductOrderObj[];
}
@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    private readonly productsService: ProductService,
    private readonly productsOrdersService: ProductsOrdersService,
  ) {}

  async createUserOrder(
    userOrder: CreateUserOrderDTO,
    user: User,
  ): Promise<Order> {
    const orderEntity: Order = Order.create();
    const { products } = userOrder;
    const initialData: Partial<OrderObject> = {
      totalPrice: 0,
      productOrders: [],
    };

    const productIds = products.map((product) => product.id);
    const allProducts = await this.productsService.findByIds(productIds);
    console.log(allProducts);
    const order = allProducts.reduce((data: Partial<any>, product): Partial<
      any
    > => {
      const { id, price } = product;
      const { quantity } = products.filter((product) => product.id == id)[0];
      const allProductPrice = price * quantity;
      const totalPrice = data.totalPrice + allProductPrice;
      const productOrder = {
        product,
        productQuantity: quantity,
      };
      const productOrders = [...data.productOrders, productOrder];
      return { totalPrice, productOrders };
    }, initialData);

    orderEntity.totalPrice = order.totalPrice;
    orderEntity.user = user;
    const savedOrder = await Order.save(orderEntity);

    await this.productsOrdersService.createMany(
      order.productOrders,
      savedOrder,
    );

    return savedOrder;
  }

  async deleteOrder(id: string): Promise<DeleteResult> {
    return this.ordersRepository.delete(id);
  }
}
